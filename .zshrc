
#######################################################
##### Gestion du prompt et des modules - antigen ######
#######################################################

# chargement d'antigen
source ~/.antigen.zsh

# modules
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-completions

antigen bundle robbyrussell/oh-my-zsh lib/
antigen bundle git 

antigen theme gp

#######################################################
##### Variables d'environnement et aliases ############
#######################################################

# User configuration
export PATH=$HOME/bin:/usr/local/bin:$PATH
export TERM=rxvt-unicode-256color

if [ -f ~/.zsh_aliases ]; then
    . ~/.zsh_aliases
fi

# You may need to manually set your language environment
export LANG=fr_FR.UTF-8
export LC_CTYPE=fr_FR.UTF-8
export LANGUAGE=fr_FR.UTF-8

#######################################################
##### Configuration de la fonction d'abbréviation #####
#######################################################

setopt extendedglob

typeset -A abbrevs
abbrevs=(
    '...' '../..'
    '....' '../../..'
    'BG' '& exit'
    'C' '| wc -l'
    'G' '|& grep --color=auto'
    'H' '| head'
    'L' '| less'
    'LL' '|& less -r'
    'N' '&>/dev/null'
    'R' '| tr A-z N-za-m'
    'SL' '| sort | less'
    'S' '| sort -u'
    'T' '| tail'
    'da' 'du -sch'
    'j' 'jobs -l'
    'co' "./configure && make && sudo make install"
    'CH' "./configure --help"
    'dir' 'ls -lSrah'
    'lad' $'ls -d .*(/)\n# only show dot-directories'
    'lsa' $'ls -a .*(.)\n# only show dot-files'
    'lss' $'ls -l *(s,S,t)\n# only files with setgid/setuid/sticky flag'
    'lsl' $'ls -l *(@[1,10])\n# only symlinks'
    'lsx' $'ls -l *(*[1,10])\n# only executables'
    'lsw' $'ls -ld *(R,W,X.^ND/)\n# world-{readable,writable,executable} files'
    'lsbig' $'ls -flh *(.OL[1,10])\n# display the biggest files'
    'lsd' $'ls -d *(/)\n# only show directories'
    'lse' $'ls -d *(/^F)\n# only show empty directories'
    'lsnew' $'ls -rl *(D.om[1,10])\n# display the newest files'
    'lsold' $'ls -rtlh *(D.om[-11,-1])\n# display the oldest files'
    'lssmall' $'ls -Srl *(.oL[1,10])\n# display the smallest files'
    'rw-' 'chmod 600'
    '600' 'chmod u+rw-x,g-rwx,o-rwx'
    'rwx' 'chmod u+rwx'
    '700' 'chmod u+rwx,g-rwx,o-rwx'
    'r--' 'chmod u+r-wx,g-rwx,o-rwx'
    '644' $'chmod u+rw-x,g+r-wx,o+r-wx\n # 4=r,2=w,1=x'
    '755' 'chmod u+rwx,g+r-w+x,o+r-w+x'
    'md' 'mkdir -p '
    'insecssh' 'ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null"'
    'insecscp' 'scp -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null"'
    'cx' 'chmod +x'
    'e' 'print -l'
    'se' 'setopt interactivecomments'
    'vuc'  'vcsh user-configuration'
    'rm'  'rm -rI --preserve-root'

    'oh'  '.antigen/repos/https-COLON--SLASH--SLASH-github.com-SLASH-robbyrussell-SLASH-oh-my-zsh.git/'
)

# Create global aliases from the abbreviations.
for abbr in ${(k)abbrevs}; do
    alias -g $abbr="${abbrevs[$abbr]}"
done

globalias() {
    local MATCH
    LBUFFER=${LBUFFER%%(#m)[_a-zA-Z0-9]#}
    LBUFFER+=${abbrevs[$MATCH]:-$MATCH}
    zle self-insert
}

zle -N globalias

# List abbreviations and abbr binding, picks out help in green.
H-Abbrevs() {
    echo "Displaying shell abbreviations"
    echo "Keybinding is:" ${$(bindabbr | grep globalias)[1]}
    for abbr in ${(ok)abbrevs}; do
        printf "%-20s: %s" $abbr ${abbrevs[$abbr]:s/
        /$fg[green] /} # Replaces newlines with spaces
        print -l $reset_color
    done
}

bindkey "  " globalias
bindkey " " magic-space 

#######################################################
################## Alias suffixés #####################
#######################################################

# texts
alias -s txt='less -rE'
# archives
alias -s zip="zipinfo"
alias -s gz="file-roller"  # because its not possible to make an alias on tar.gz its better to make a general alias for all gz-files 
# gfx
alias -s xcf="gimp-remote"
alias -s png="xv"
alias -s gif="xv"
alias -s jpg="xv"
# media files
alias -s pdf="evince"
# code
alias -s php="/usr/share/vim/vim62/macros/less.sh" # less with vim's syntax-hilighting
alias -s c="/usr/share/vim/vim62/macros/less.sh"
alias -s html="/usr/share/vim/vim62/macros/less.sh"

#######################################################
################## raccourci clavier ##################
#######################################################

bindkey '[C' forward-word                        # [Ctrl-RightArrow] - move forward one word
bindkey '[D' backward-word                       # [Ctrl-LeftArrow] - move backward one word
